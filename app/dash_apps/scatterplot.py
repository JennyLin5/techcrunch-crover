import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import base64
from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
app = DjangoDash('scatterplot')

# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([
    html.H3("Mean Temperature Moisture Scatter Plot"),
     dcc.Dropdown(id="slct_array",
                 options=[
                     {"label": "temperature", "value": "temperature"},
                     {"label": "moisture", "value": "moisture"}
                      ],
                 multi=False,
                 value="moisture",
                 style={'width': "40%"}
                 ),
    html.Div(id='output_container', children=[]), 

    dcc.Graph(id='graph'),
    
])

# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
# Define callback to update graph
@app.callback(
    [Output('graph', 'figure'),
    Output(component_id='output_container', component_property='children')],
    [Input("slct_array", "value")]
)
def update_figure(option_slctd):
    container=("The array chosen by user was: {}".format(option_slctd))
    table = pd.DataFrame()
    if option_slctd=="temperature":
        for i in range(20):
            temp_table = pd.DataFrame()
            data_path = os.path.join(os.getcwd(), '../../maps/', 'temp_map_array_'+str(i)+'.txt')
            df = pd.read_csv(data_path, delimiter = ',', header=None)
            a = df.mean(axis=1)
            table[i] = a.values.flatten()
        fig = go.Figure(px.scatter(table ))
        fig.update_layout(
            title="Mean Temperature Scatter Plot",
            yaxis_title="°C")
        fig.add_trace(go.Scatter(
            y=table.max(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Max. Temperature"
        ))        
        fig.add_trace(go.Scatter(
            y=table.min(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Min. Temperature"
        ))
        fig.add_trace(go.Scatter(
            y=table.mean(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Mean Temperature"
        ))

    else:
        table = pd.DataFrame()
        for i in range(20):
            data_path = os.path.join(os.getcwd(), '../../maps/', 'moist_map_array_'+str(i)+'.txt')
            df = pd.read_csv(data_path, delimiter = ',', header=None)
            a = df.mean(axis=1)
            table[i] = a.values.flatten()
        fig = go.Figure(px.scatter(table ))
        fig.update_layout(
            title="Mean Moisture Scatter Plot",
            yaxis_title="%")
        fig.add_trace(go.Scatter(
            y=table.max(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Max. Moisture"
        ))        
        fig.add_trace(go.Scatter(
            y=table.min(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Min. Moisture"
        ))        
        fig.add_trace(go.Scatter(
            y=table.mean(axis=1).values.flatten(),
            x=list(range(0, 39)),
            name="Mean. Moisture"
        ))
 
    fig.update_layout(
        xaxis_title="Column",
        legend_title="Arrays/Layers",
#         font=dict(
#             family="Courier New, monospace",
#             size=18,
#             color="RebeccaPurple"
#         )
    )
 
    return fig, container

# ------------------------------------------------------------------------------
