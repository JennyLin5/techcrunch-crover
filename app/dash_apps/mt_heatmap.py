import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
from plotly.subplots import make_subplots

import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import base64
from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
app = DjangoDash('mt_heatmap')

from ..helper import find_table,get_sites_names
# ------------------------------------------------------------------------------
x=get_sites_names()
x.append("20") # for demo purposes 20 = v10 time6
x.append("21") # for demo 21 = v11 time6
app.layout = html.Div([
    html.H3("3D Map of Conditions Within a Grain Store"),
    html.Label("Grain Store",style={'display':'inline-flex','font-size':'20px','padding-right':'20px'}),
    dcc.Dropdown(id="unit_array",
                 options=[{'label': j, 'value': j} for j in x],
                 multi=False,
                 value="4",
                 style={'width': '30%', 'display' : 'inline-block','font-size':'18px','margin-bottom':'-5px'}
                 ),                     
    html.Div(id='output_container', children=[]), 

    dcc.Graph(id='graph'),
]
)

# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output('graph', 'figure'),
    Output(component_id='output_container', component_property='children')
    ],
    [Input("unit_array", "value")]
)

def update_figure(unit):
    coord='48.972/169.3274'
    container=("")

    if unit == "20":
        pathM=os.path.join(BASE_DIR, 'data','moist_v10_6.txt')
        pathT=os.path.join(BASE_DIR, 'data','temp_v10_6.txt')
        dim=20 #y=20 z-20

        dfM = pd.read_csv(pathM, header=None)
        dfT = pd.read_csv(pathT, header=None)

        a = dfM.to_numpy()
        b = dfT.to_numpy()
        length = len(a)

        a_shaped = a.reshape(dim, int(length/dim))
        a_shaped_transposed = a_shaped.transpose()
        b_shaped = b.reshape(dim, int(length/dim))
        b_shaped_transposed = b_shaped.transpose()

        moist_table=pd.DataFrame(a_shaped_transposed)
        temp_table=pd.DataFrame(b_shaped_transposed)
    elif unit == "21":
        pathM=os.path.join(BASE_DIR, 'data','moist_v11_6.txt')
        pathT=os.path.join(BASE_DIR, 'data','temp_v11_6.txt')
        dim=20 #y=20 z-20

        dfM = pd.read_csv(pathM, header=None)
        dfT = pd.read_csv(pathT, header=None)

        a = dfM.to_numpy()
        b = dfT.to_numpy()
        length = len(a)

        a_shaped = a.reshape(10, int(length/10))
        a_shaped_transposed = a_shaped.transpose()
        b_shaped = b.reshape(10, int(length/10))
        b_shaped_transposed = b_shaped.transpose()

        moist_table=pd.DataFrame(a_shaped_transposed)
        temp_table=pd.DataFrame(b_shaped_transposed)
    else:

        temp_table=find_table(coord, unit, "temp")
        moist_table=find_table(coord, unit, "moist")
        dim = temp_table['xcoord'].max()
        print(dim)
        moist_table=moist_table.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
        # head=list(range(0, int(dim/2)))
        # moist_table.columns = head    
        # moist_table = moist_table.astype(float)
        moist_table=moist_table.head(dim*dim)
        # print(moist_table)
        temp_table=temp_table.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
        # head=list(range(0, int(dim/2)))
        # temp_table.columns = head    
        # temp_table = temp_table.astype(float)
        temp_table=temp_table.head(dim*dim)

    temp_color=['#005bb8', '#8ab900','#F05030']
    # temp_color=[(1,'#005bb8'), (22,'#8ab900'),(300,'#F05030')]
    temp_unit = "°C"
    temp_name="Temperature"


    moist_color=['#ffffd9','#41b6c4','#081d58']
    # moist_color=[(1,'#ffffd9'),(15.5,'#41b6c4'),(190,'#081d58')]
    moist_unit = "%"
    moist_name="Moisture"

   

    # print(temp_table)
    mz=[(np.matrix(moist_table[i].tolist())).reshape( int(dim), int(dim)) for i in moist_table]
    tz=[(np.matrix(temp_table[i].tolist())).reshape( int(dim), int(dim)) for i in temp_table]
    
    fig = make_subplots(
        rows=1, cols=2,subplot_titles=("Moisture","Temperature"),
        specs=[[{'type': 'surface'}, {'type': 'surface'}],
    ])
    
    # tmax=(max(map(max, tz[0][0])).max())+dim*(len(temp_table.columns)+1)
    # print(tmax)
    for i in range(len(moist_table.columns)):
        r=mz[i]
        #colorbar=dict(x=.35,thickness=20, tickvals=[12, 19], ticktext=['Low', 'High'], outlinewidth=0),
        fig.add_trace(go.Surface(z=mz[i]+dim*(i+1), text=mz[i], hoverinfo='text', meta=['Moisture',i], hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta[1]}m <br><b>%{meta[0]}: %{text}</b>'+moist_unit,
                                 colorscale=moist_color, surfacecolor=mz[i], cmin=10, cmax=19,
                                 colorbar=dict(x=.42,thickness=20,title='(%)'),opacityscale="max",
                                 showscale=True, opacity=1, name="{}".format(moist_table.columns[i])),row=1, col=1,)


    for i in range(len(temp_table.columns)):
        r=tz[i]
        fig.add_trace(go.Surface(z=tz[i]+dim*(i+1), text=tz[i], hoverinfo='text', meta=['Temperature',i], hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta[1]}m <br><b> %{meta[0]}: %{text}</b>'+temp_unit,
                                 colorscale=temp_color, surfacecolor=tz[i], cmin=2, cmax=25,
                                 colorbar=dict(x=.95, thickness=20, title='(°C) '),opacityscale="max",
                                 showscale=True, opacity=1, name="{}".format(temp_table.columns[i])),row=1, col=2,)
    
   
    # Add silo image overlay

    image_filename = os.getcwd() + '/static/images/silo.png' 
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())
    fig.add_layout_image(
        dict(
            source=('data:image/png;base64,{}'.format(encoded_image.decode())),
            xref="x",
            yref="y",
            x=-7.1,
            y=6,
            sizex=10,
            sizey=8,
            sizing="stretch",
            opacity=0.3,
            layer="above")
    )
    image_filename = os.getcwd() + '/static/images/silo.png' 
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())
    fig.add_layout_image(
        dict(
            source=('data:image/png;base64,{}'.format(encoded_image.decode())),
            xref="x",
            yref="y",
            x=1.7,
            y=6.1,
            sizex=10,
            sizey=8,
            sizing="stretch",
            opacity=0.3,
            layer="above")
    )

    xy_ticks =[p/2 for p in range(0, dim)]
    print(xy_ticks)
    fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
    fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)

    fig.update_layout( scene_aspectmode='cube',autosize=False,title_x=0.5, coloraxis1=dict(colorscale=moist_color,),
                  height=350, margin={"l": 10, "r": 10, "t": 30, "b": 10},
                  scene_camera = dict(
                        eye=dict(x=1.65, y=1.65, z=1.25)
                    ),
                  xaxis =  { 'showgrid': False,'zeroline':False},
                  yaxis = {'showgrid': False,'zeroline':False},
                  scene = dict(
                    xaxis = dict(range=[0,int(dim)], title='length (m)',showgrid=False,zeroline=False,showline=False,),
                    yaxis = dict(range=[0,int(dim)], title='width (m)',showgrid=False, ticktext=xy_ticks),
                    zaxis = dict(
                           showticklabels=False, title='height (m)',showgrid=False,zeroline=False,
                        )))
    fig.update_layout( scene2_aspectmode='cube',autosize=False,title_x=0.5, coloraxis2=dict(colorscale=temp_color,),
                  height=350, margin={"l":0, "r": 10, "t": 30, "b": 10},
                  scene2_camera = dict(
                        eye=dict(x=1.65, y=1.65, z=1.25),
                  ),
                  xaxis =  { 'showgrid': False,'zeroline':False},
                  yaxis = {'showgrid': False,'zeroline':False},
                    scene2 = dict(
                        xaxis = dict(range=[0,int(dim)], title='length (m)',showgrid=False,zeroline=False,showline=False,),
                        yaxis = dict(range=[0,int(dim)], title='width (m)',showgrid=False),
                        zaxis = dict(
                            showticklabels=False, title='height (m)',showgrid=False,zeroline=False
                            )))

    return fig, container

# ------------------------------------------------------------------------------
