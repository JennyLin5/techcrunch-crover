import os
import pandas as pd
import base64
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from django_plotly_dash import DjangoDash

from ..helper import find_table, unit_table_mean,get_sites_names
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = DjangoDash('mean_time_threshold')



# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([
    html.H3("Moisture/Temperature above threshold"),
    html.Div([
        
        html.Label('Select data type',style={'width': '12%', 'height':'100%','display':'inline-block'}),
        dcc.Dropdown(id="slct_array",
                    options=[
                        #        {'label': f'blah-blah {i}', 'value': i} for i in range(10)
                        {"label": "temperature", "value": "temp"},
                        {"label": "moisture", "value": "moist"}
                        ],
                    multi=False,
                    value="moist",
                    style={'width': '38%', 'height':'100%','display':'inline-block',}
        ),
        
        html.Label('Select site coordinates',style={'width': '12%', 'height':'100%','display':'inline-block'}),
        dcc.Dropdown(id="coord_array",
                    options=[
                        {"label": "'48.972/169.3274'", "value": "48.972/169.3274"},
                        ],
                    multi=False,
                    value="48.972/169.3274",
                    style={'width': '38%', 'height':'100%','display':'inline-block'}
        ),

        html.Label('Select unit number of site',style={'width': '12%', 'height':'100%','display':'inline-block'}),             
        dcc.Dropdown(id="unit_array",                 
                    options=[{'label': j, 'value': j} for j in get_sites_names()],
                    multi=False,
                    value="1",
                    style={'width': '38%', 'height':'100%','display':'inline-block'}
        ),

        html.Label('Input threshold',style={'width': '12%', 'height':'100%','display':'inline-block'}),
        dcc.Input(id="input_threshold",
                    type="number", placeholder="Default",
                    min=0, max=100, step=.5,
                    style={'width': '10%', 'height': '32px','display':'inline-block'}
        ),    

        html.Div(id='output_container', children=[], style={'columnCount': 1,'zIndex': '-1'}),

        dcc.Graph(id='graph'),
    ],)
    # style={'columnCount': 4,'zIndex': '1111'}),
 
# style={'padding':'5px','zIndex': '-100'}
])

# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
# Define callback to update graph
@app.callback(
    [Output('graph', 'figure'),
    Output(component_id='output_container', component_property='children')],
    [Input("slct_array", "value"),Input("coord_array", "value"),Input("unit_array", "value"), Input("input_threshold","value")]
)

def update_figure(slct_array,coord,unit, input_threshold):
    #set default threshold
    if (input_threshold) is None:
        if slct_array == "moist":
            input_threshold = 14.5
        else:
            input_threshold = 12

    container=("The array chosen by user was: {}, coord: {}, unit: {}, input threshold: {}.".format(slct_array, coord, unit, input_threshold))
    print(container)
    print(input_threshold)
    print(coord)
    print(unit)
    threshold=input_threshold
    # try: 
    
    data=find_table(coord, unit, slct_array)
    
    # except:
    #     data=[]
    #     print("table is not correct")
    # print(data)
    result=pd.DataFrame()
    for u in data:
        x = unit_table_mean( coord, u,data)
        if float(x['mean'].mean()) > threshold:
            result=result.append(x)
    print(result)
    # Plot
    if result.empty:
        fig = go.Figure(px.scatter())
        print("empty")

    else:
        fig = go.Figure(px.scatter(result, x='timestamp',y='mean'))
        fig.add_trace(go.Scatter(
            x=result['timestamp'], y=result['mean'],
            name="Mean Temperature"
        ))
        # return result.plot(x='timestamp',y='mean',marker='o', c='red')
    
    
    return fig, container

# ------------------------------------------------------------------------------
