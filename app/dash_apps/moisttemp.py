import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import base64

from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

app = DjangoDash('moisttemp')

# ------------------------------------------------------------------------------
# Import and clean data (importing csv into pandas)

# Import moisture data
moist_table = pd.DataFrame()
for i in range(20):
    data_path = os.path.join(BASE_DIR, 'maps', 'moist_map_array_'+str(i)+'.txt')
    df = pd.read_csv(data_path, delimiter = ',', header=None)
    moist_table[i] = df.values.flatten()

# Import temperature data
temp_table = pd.DataFrame()
for i in range(20):
    data_path = os.path.join(BASE_DIR, 'maps', 'temp_map_array_'+str(i)+'.txt')
    df = pd.read_csv(data_path, delimiter = ',', header=None)
    temp_table[i] = df.values.flatten()


# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([

    #html.H1("Moisture dashboard", style={'text-align': 'center'}),
      dcc.Dropdown(id="slct_array",
                 options=[
                     {"label": "temperature", "value": "temperature"},
                     {"label": "moisture", "value": "moisture"}
                      ],
                 multi=False,
                 value="moisture",
                 style={'width': "40%"}
                 ),
    html.Div(id='output_container', children=[]), 
    dcc.Graph(id='plot', figure={}),
    html.Br(),


])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'), #output 1 - year
     Output(component_id='plot', component_property='figure')], #output 2 - figure
    [Input(component_id='slct_array', component_property='value')] #slider
)

def update_graph(option_slctd):
    print(option_slctd)
    container = "Option Selected: {}".format(option_slctd)

    if option_slctd=="temperature":
        table=temp_table
    else:
        table=moist_table

    z=[(np.matrix(table[i].tolist())).reshape(40,40) for i in range(20)]
    data=[]
    
    data.append(go.Surface(z=(np.matrix(table.mean(axis=1).tolist())).reshape(40,40), showscale=True, opacity=0.9))

    print(table.columns)
    for i in range(len(table.columns)):
        
        data.append(go.Surface(z=z[i]+3*i, showscale=False, opacity=0.9, name="array {}".format(table.columns[i])))
   
    fig = go.Figure(data=data)

    # Add silo image overlay
    image_filename = os.getcwd() + '/static/images/silo.png' 
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())
    fig.add_layout_image(
        dict(
            source=('data:image/png;base64,{}'.format(encoded_image.decode())),
            xref="x",
            yref="y",
            x=-4.5,
            y=7,
            sizex=13.5,
            sizey=10,
            sizing="stretch",
            opacity=0.3,
            layer="above")
    )
    fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
    fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)


    fig.update_layout( autosize=False,  #title_text=option_slctd,
                  width=750, height=400,
                  margin=dict(l=100, r=100, b=50, t=50),
                #   zaxis=dict(ticks='', showticklabels=False),
                  scene = dict(
                    zaxis_title='Layers/Array',))

    return container, fig #output1-year #output2-figure


# ------------------------------------------------------------------------------
